// AssetTemplates Reducer

const assetTemplatesReducerDefaultState = [];

export default (state = assetTemplatesReducerDefaultState, action) => {
  switch (action.type) {
    case 'ADD_ASSETTEMPLATE':
      return [
        ...state,
        action.assetTemplate
      ];
    case 'REMOVE_ASSETTEMPLATE':
      return state.filter(({ id }) => id !== action.id);
    case 'EDIT_ASSETTEMPLATE':
      return state.map((assetTemplate) => {
        if (assetTemplate.id === action.id) {
          return {
            ...assetTemplate,
            ...action.updates
          };
        } else {
          return assetTemplate;
        };
      });
    case 'SET_ASSETTEMPLATES':
      return action.assetTemplates;
    default:
      return state;
  }
};
