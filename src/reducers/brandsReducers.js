// AssetTemplates Reducer

const brandsReducerDefaultState = [];

export default (state = brandsReducerDefaultState, action) => {
  switch (action.type) {
    case 'ADD_BRAND':
      return [
        ...state,
        action.brand
      ];
    case 'REMOVE_BRAND':
      return state.filter(({ id }) => id !== action.id);
    case 'EDIT_BRAND':
      return state.map((brand) => {
        if (brand.id === action.id) {
          return {
            ...brand,
            ...action.updates
          };
        } else {
          return brand;
        };
      });
    case 'SET_BRANDS':
      return action.brands;
    default:
      return state;
  }
};
