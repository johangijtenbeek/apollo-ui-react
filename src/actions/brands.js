import database from '../firebase/firebase';

export const setBrands = (brands) => ({
  type: 'SET_BRANDS',
  brands
});


export const startSetBrands = () => {
    return (dispatch) => {
      return database.ref('Brands').once('value').then((snapshot) => {
        const brands = [];
        snapshot.forEach((childSnapshot) => {
          brands.push({
            id: childSnapshot.key,
            ...childSnapshot.val()
          });
        });
  
        dispatch(setBrands(brands));
      }).catch((e) => { debugger; });
    };
  };

export const addBrand = (brand) => ({
  type: 'ADD_BRAND',
  brand
});

export const startAddBrand = (brand = {}) => {
  return (dispatch) => {
    return database.ref('Brands').push(brand).then((ref) => {
      dispatch(addBrand({
        id: ref.key,
        ...brand
      }));
    });
  };
};

export const editBrand = (id, updates) => ({
  type: 'EDIT_BRAND',
  id,
  updates
});

export const startEditBrand = (id, updates) => {
  return (dispatch) => {
    return database.ref(`Brands/${id}`).update(updates).then(() => {
      dispatch(editBrand(id, updates));
    });
  };
};


// REMOVE_AssetTemplate
export const removeBrand = ({ id } = {}) => ({
  type: 'REMOVE_BRAND',
  id
});

export const startRemoveBrand = ({ id } = {}) => {
  return (dispatch) => {
    return database.ref(`Brands/${id}`).remove().then(() => {
      dispatch(removeBrand({ id }));
    });
  };
};