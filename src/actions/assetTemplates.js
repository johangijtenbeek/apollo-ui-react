import database from '../firebase/firebase';

// ADD_AssetTemplate
export const addAssetTemplate = (assetTemplate) => ({
  type: 'ADD_ASSETTEMPLATE',
  assetTemplate
});

export const startAddAssetTemplate = (assetTemplateData = {}) => {
  return (dispatch) => {
    const {
      name = '',
      location = '',
      brandId = '',
      type = '',
      size = '' 
    } = assetTemplateData;
    const assetTemplate = { name, location, brandId, type, size };

    return database.ref('AssetTemplates').push(assetTemplate).then((ref) => {
      dispatch(addAssetTemplate({
        id: ref.key,
        ...assetTemplate
      }));
    });
  };
};

// REMOVE_AssetTemplate
export const removeAssetTemplate = ({ id } = {}) => ({
  type: 'REMOVE_ASSETTEMPLATE',
  id
});

export const startRemoveAssetTemplate = ({ id } = {}) => {
  return (dispatch) => {
    return database.ref(`AssetTemplates/${id}`).remove().then(() => {
      dispatch(removeAssetTemplate({ id }));
    });
  };
};

// EDIT_AssetTemplate
export const editAssetTemplate = (id, updates) => ({
  type: 'EDIT_ASSETTEMPLATE',
  id,
  updates
});

export const startEditAssetTemplate = (id, updates) => {
  return (dispatch) => {
    return database.ref(`AssetTemplates/${id}`).update(updates).then(() => {
      dispatch(editAssetTemplate(id, updates));
    });
  };
};

// SET_AssetTemplateS
export const setAssetTemplates = (assetTemplates) => ({
  type: 'SET_ASSETTEMPLATES',
  assetTemplates
});

export const startSetAssetTemplates = () => {
  return (dispatch) => {
    return database.ref('AssetTemplates').once('value').then((snapshot) => {


      let assetTemplates = [];
      const brands = [];

      snapshot.forEach((childSnapshot) => {
        assetTemplates.push({
          id: childSnapshot.key,
          ...childSnapshot.val()
        });
      });

      database.ref('Brands').once('value').then((snapshot) => {        
          snapshot.forEach((childSnapshot) => {
            brands.push({
              id: childSnapshot.key,
              ...childSnapshot.val()
          });
        });   

      assetTemplates = assetTemplates.map(assetTemplate =>
      {
        const brand = brands.find((brand) => brand.id === assetTemplate.brandId);
        const brandName = brand === undefined ? '' : brand.brandName;

        return {
          ...assetTemplate, 
          brandName: brandName
        };
      });

      dispatch(setAssetTemplates(assetTemplates));
    });      
    }).catch((e) => { debugger; });
  };
};


