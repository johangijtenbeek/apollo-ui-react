import React from 'react';
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom';

import AssetManagerDashboardPage from '../components/AssetManager/AssetManagerDashboardPage';

import AddAssetTemplatePage from '../components/AssetManager/AssetTemplateComponents/AddAssetTemplatePage';
import EditAssetTemplatePage from '../components/AssetManager/AssetTemplateComponents/EditAssetTemplatePage';

import HelpPage from '../components/HelpPage';
import NotFoundPage from '../components/NotFoundPage';
import Header from '../components/Header';
import BrandsDashboard from '../components/AssetManager/BrandComponents/BrandsDashboard';

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Header />
      <Switch>
        <Route path="/" component={AssetManagerDashboardPage} exact={true} />
        <Route path="/assettemplates/create" component={AddAssetTemplatePage} />
        <Route path="/assettemplates/edit/:id" component={EditAssetTemplatePage} />
        <Route path="/brands/" component={BrandsDashboard} />
        <Route path="/help" component={HelpPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default AppRouter;
