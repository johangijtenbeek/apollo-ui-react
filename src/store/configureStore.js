import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import assetTemplatesReducer from '../reducers/assetTemplates';
import filtersReducer from '../reducers/filters';
import thunk from 'redux-thunk';
import brandsReducers from '../reducers/brandsReducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
  const store = createStore(
    combineReducers({
      brands: brandsReducers,
      assetTemplates: assetTemplatesReducer,
      filters: filtersReducer
    }),
    composeEnhancers(applyMiddleware(thunk))
  );

  return store;
};
