import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';
import { startSetAssetTemplates } from './actions/assetTemplates';
import { startSetBrands } from './actions/brands'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';
import database from './firebase/firebase';

import 'normalize.css/normalize.css';
import './styles/styles.scss';
import 'react-dates/lib/css/_datepicker.css';
import './firebase/firebase';
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom';

import DatagridContainer from './components/Common/Datagrid/DatagridContainer';


const store = configureStore();


const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: purple,
    secondary: green,
  },
  status: {
    danger: 'orange',
  },
})

var data = [
  { name: 'TestA', function: 'devA', wage: 15000, id: 0, changedColumns: []},
  { name: 'TestB', function: 'devB', wage: 1500, id: 1, changedColumns: []},
  { name: 'TestC', function: 'devC', wage: 3000, id: 2, changedColumns: []},
  { name: 'TestD', function: 'devD', wage: 85000, id: 3, changedColumns: []},
  { name: 'TestE', function: 'devE', wage: 225000, id: 4, changedColumns: []},
  { name: 'TestF', function: 'devF', wage: 5000, id: 5, changedColumns: []},
  { name: 'TestG', function: 'devG', wage: 600, id: 6, changedColumns: []},
  { name: 'TestH', function: 'devH', wage: 15000, id: 7, changedColumns: []}
];

var columns = [
  { propertyName: 'name', numeric: false, disablePadding: true, label: 'Naam' },
  { propertyName: 'function', numeric: false, disablePadding: true, label: 'Functie' },
  { propertyName: 'wage', numeric: true, disablePadding: true, label: 'Int' },
]

const table = () => ( 
<DatagridContainer   
  order={'asc'}
  orderBy= {'brandName'}
  data= {data}
  rowsPerPage={5}
  columns={columns} 
  selectable={false}
/>);

const jsx = (
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <BrowserRouter>
        <Switch>
            <Route path="/" component={table}/>
        </Switch>
      </BrowserRouter>
    </MuiThemeProvider>
  </Provider>
);

/*const jsx = (
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <AppRouter />
    </MuiThemeProvider>
  </Provider>
);*/

ReactDOM.render(jsx, document.getElementById('app'));

/*
ReactDOM.render(<p>Loading...</p>, document.getElementById('app'));

store.dispatch(startSetAssetTemplates())
.then(() => {
  store.dispatch(startSetBrands()).then(
    () => { 
      ReactDOM.render(jsx, document.getElementById('app'));
      })
  });*/
