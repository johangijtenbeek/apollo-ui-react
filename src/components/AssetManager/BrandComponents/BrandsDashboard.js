import React from 'react';
import { connect } from 'react-redux';
import { startAddBrand } from '../../../actions/brands';
import PropTypes from 'prop-types';
import BrandForm from './BrandForm';
import EnhancedTable from '../../Common/Table/EnhancedTable';

class BrandsDashboard extends React.Component {

  onSubmit = (brand) => {
    this.props.startAddBrand(brand);
    //this.props.history.push('/brands');
  };

  componentWillReceiveProps(props) {     
    let counter = 0; 
    this.setState( {
      brands: props.brands,
      brandRows:  props.brands.map(brand =>{
        counter += 1;
        return { id: counter, ...brand };
      })
    });
  }

  render() { 

    const tableConfig = {
      order: 'asc',
      orderBy: 'brandName',
      selected: [],
      rows: this.props.brandRows,
      page: 0,
      rowsPerPage: 15,
      columns: [
        { id: 'brandName', numeric: false, disablePadding: true, label: 'Merk naam' },
      ],
      onRemove: this.onRemove
    }

    return (
      <div>
        <BrandForm onSubmit={this.onSubmit}/>

        <EnhancedTable tableConfig={tableConfig} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  let counter = 0;
  return {
    brands: state.brands,
    brandRows:  state.brands.map(brand =>{
      counter += 1;
      return { id: counter, ...brand };
    })

  };
};

const mapDispatchToProps = (dispatch) => ({
  startAddBrand: (brand) => dispatch(startAddBrand(brand))
});

export default connect(mapStateToProps, mapDispatchToProps)(BrandsDashboard);
