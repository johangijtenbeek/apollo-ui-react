import React from 'react';
import { connect } from 'react-redux';
import BrandForm from './BrandForm';
import { startEditBrand, startRemoveBrand } from '../../../actions/brands';

export class EditBrandPage extends React.Component {
  
  onSubmit = (brand) => {
    this.props.startEditBrand(this.props.brand.id, brand);
    this.props.history.push('/');
  };
  onRemove = () => {
    this.props.startRemoveBrand({ id: this.props.brand.id });
    this.props.history.push('/');
  };

  render() {
    return (
      <div>
        <BrandForm          
          brand={this.props.brand}
          onSubmit={this.onSubmit}
        />
        <button onClick={this.onRemove}>Remove</button>
      </div>
    );
  }
};

const mapStateToProps = (state, props) => { 
  return {
    brand: state.brands.find((brand) => brand.id === props.match.params.id)
  }
};

const mapDispatchToProps = (dispatch, props) => ({
  startEditBrand: (id, assetTemplate) => dispatch(startEditBrand(id, Brand)),
  startRemoveBrand: (data) => dispatch(startRemoveBrand(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditBrandPage);
