import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

class BrandForm extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      brandName: props.brand ? props.brand.name : '',
      error: ''
    };
  }
    
  componentWillReceiveProps(props) {      
    this.setState({brandOptions: props.brandOptions});
}

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    if (!this.state.brandName)  {
      this.setState(() => ({ error: 'Vul alle velden in' }));
    } else {
      this.setState(() => ({ error: '' }));      
      this.props.onSubmit({
        brandName: this.state.brandName
      });
    }
  };

  render() {
    const { classes } = this.props;

    return (
    <div>
    {this.state.error && <p>{this.state.error}</p>}
      <form className={classes.container} noValidate autoComplete="off" onSubmit={this.onSubmit}>
      <TextField
        id="brandName"
        label="Name"
        className={classes.textField}
        value={this.state.brandName}
        onChange={this.handleChange('brandName')}
        margin="normal"
      />
      <Button variant="contained"  type="submit" color="primary" className={classes.button}>
          Opslaan
        </Button>
    </form>
    </div>
    )
  }
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
});

BrandForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BrandForm);
