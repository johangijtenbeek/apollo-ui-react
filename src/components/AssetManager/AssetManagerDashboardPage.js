import React from 'react';
import AssetTemplateList from './AssetTemplateComponents/AssetTemplateList';


const AssetManagerDashboardPage = () => (
  <div>
    <AssetTemplateList />
  </div>
);

export default AssetManagerDashboardPage;
