import React from 'react';
import { connect } from 'react-redux';
import AssetTemplateForm from './AssetTemplateForm';
import { startAddAssetTemplate } from '../../../actions/assetTemplates';
import PropTypes from 'prop-types';

class AddAssetTemplatePage extends React.Component {
  
  onSubmit = (AssetTemplate) => {
    this.props.startAddAssetTemplate(AssetTemplate);
    this.props.history.push('/');
  };

  mapBrandsToOptions = (brands) => 
  {
    const brandOptions = brands.map(brand => { return { key: brand.id, value: brand.id, label: brand.brandName }});
    return brandOptions;
  }

  render() { 
    const props = this.props;
    return (
        <AssetTemplateForm 
          brandOptions={ this.mapBrandsToOptions(this.props.brands) }
          onSubmit={this.onSubmit}
        />
    );
  }
}

AddAssetTemplatePage.propTypes = {
  brands: PropTypes.array.isRequired
};

const mapStateToProps = (state, ownProps) => ({
    brands: state.brands
  });

const mapDispatchToProps = (dispatch) => ({
  startAddAssetTemplate: (AssetTemplate) => dispatch(startAddAssetTemplate(AssetTemplate))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddAssetTemplatePage);
