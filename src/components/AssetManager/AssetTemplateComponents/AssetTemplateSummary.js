import React from 'react';
import { connect } from 'react-redux';
import numeral from 'numeral';
import selectAssetTemplates from '../../selectors/assetTemplates';
import selectAssetTemplatesTotal from '../../selectors/assetTemplates-total';

export const AssetTemplateSummary = ({ assetTemplateCount, assetTemplatesTotal }) => {
  const assetTemplateWord = assetTemplateCount === 1 ? 'AssetTemplate' : 'AssetTemplates' ;
  const formattedAssetTemplatesTotal = numeral(assetTemplatesTotal / 100).format('$0,0.00');
  
  return (
    <div>
      <h1>Viewing {assetTemplateCount} {assetTemplateWord} totalling {formattedAssetTemplatesTotal}</h1>
    </div>
  );
};

const mapStateToProps = (state) => {
  const visibleAssetTemplates = selectAssetTemplates(state.assetTemplates, state.filters);

  return {
    assetTemplateCount: visibleAssetTemplates.length,
    assetTemplatesTotal: selectAssetTemplatesTotal(visibleAssetTemplates)
  };
};

export default connect(mapStateToProps)(AssetTemplateSummary);
