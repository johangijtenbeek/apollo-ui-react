import React from 'react';
import { connect } from 'react-redux';
import AssetTemplateForm from './AssetTemplateForm';
import { startEditAssetTemplate, startRemoveAssetTemplate } from '../../../actions/assetTemplates';

export class EditAssetTemplatePage extends React.Component {
  
  onSubmit = (assetTemplate) => {
    this.props.startEditAssetTemplate(this.props.assetTemplate.id, assetTemplate);
    this.props.history.push('/');
  };
  onRemove = () => {
    this.props.startRemoveAssetTemplate({ id: this.props.assetTemplate.id });
    this.props.history.push('/');
  };

  mapBrandsToOptions = (brands) => 
  {
    const brandOptions = brands.map(brand => { return { key: brand.id, value: brand.id, label: brand.name }});
    return brandOptions;
  };

  render() {
    return (
      <div>
        <AssetTemplateForm          
          AssetTemplate={this.props.assetTemplate}
          brandOptions={ this.mapBrandsToOptions(this.props.brands) }
          onSubmit={this.onSubmit}
        />
        <button onClick={this.onRemove}>Remove</button>
      </div>
    );
  }
};

const mapStateToProps = (state, props) => { 
  return {    
    brands: state.brands,
    assetTemplate: state.assetTemplates.find((assetTemplate) => assetTemplate.id === props.match.params.id)
  }
};

const mapDispatchToProps = (dispatch, props) => ({
  startEditAssetTemplate: (id, assetTemplate) => dispatch(startEditAssetTemplate(id, assetTemplate)),
  startRemoveAssetTemplate: (data) => dispatch(startRemoveAssetTemplate(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditAssetTemplatePage);
