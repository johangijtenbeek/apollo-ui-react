import React from 'react';
import { connect } from 'react-redux';

import EnhancedTable from '../../Common/Table/EnhancedTable';

import selectAssetTemplates from '../../../selectors/assetTemplates';
import { startRemoveAssetTemplate } from '../../../actions/assetTemplates';

let counter = 0;

export class AssetTemplateList extends React.Component {

  onRemove = (e, id) => {
    debugger;
    e.preventDefault();
    this.props.startRemoveAssetTemplate({ id: id });
  };

  render() {    
    const data = this.props.assetTemplates.map(assetTemplate => {
      counter += 1;
      return { id: counter, ...assetTemplate };
    });
  
    const tableConfig = {
      order: 'asc',
      orderBy: 'name',
      selected: [],
      rows: data,
      page: 0,
      rowsPerPage: 5,
      columns: [
        { id: 'name', numeric: false, disablePadding: true, label: 'Materiaal naam' },
        { id: 'location', numeric: false, disablePadding: false, label: 'Locatie' },
        { id: 'brandName', numeric: false, disablePadding: false, label: 'Merk' },
        { id: 'type', numeric: false, disablePadding: false, label: 'Type' },
        { id: 'size', numeric: false, disablePadding: false, label: 'Maat' },    
        { id: 'controls', numeric: false, disablePadding: false, label: '' },
      ],
      onRemove: this.onRemove
    }

    return (
    <div>
      <EnhancedTable tableConfig={tableConfig} ></EnhancedTable>      
    </div>)
  };
}

const mapStateToProps = (state) => {
  return {
    assetTemplates: selectAssetTemplates(state.assetTemplates, state.filters)
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  startRemoveAssetTemplate: (data) => dispatch(startRemoveAssetTemplate(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(AssetTemplateList);
