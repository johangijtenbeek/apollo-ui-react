import React from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';
import { SingleDatePicker } from 'react-dates';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import brandsReducers from '../../../reducers/brandsReducers';
import Button from '@material-ui/core/Button';

class AssetTemplateForm extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      name: props.AssetTemplate ? props.AssetTemplate.name : '',
      location: props.AssetTemplate ? props.AssetTemplate.location : '',
      brandId: props.AssetTemplate ? props.AssetTemplate.brandId : '',
      type: props.AssetTemplate ? props.AssetTemplate.type : '',
      size: props.AssetTemplate ? props.AssetTemplate.size : '',
      brandOptions: props.brandOptions,
      /*amount: props.AssetTemplate ? (props.AssetTemplate.amount / 100).toString() : '',
      createdAt: props.AssetTemplate ? moment(props.AssetTemplate.createdAt) : moment(),
      calendarFocused: false,*/
      error: ''
    };
  }
    
  componentWillReceiveProps(props) {      
    this.setState({brandOptions: props.brandOptions});
}

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    if (!this.state.name || !this.state.brandId || !this.state.type || !this.state.size || !this.state.location)  {
      this.setState(() => ({ error: 'Vul alle velden in' }));
    } else {
      this.setState(() => ({ error: '' }));      
      this.props.onSubmit({
        name: this.state.name,
        location: this.state.location,
        brandId: this.state.brandId,
        type: this.state.type,
        size: this.state.size,

        /*amount: parseFloat(this.state.amount, 10) * 100,
        createdAt: this.state.createdAt.valueOf(),
        note: this.state.note*/
      });
    }
  };

  render() {
    const { classes } = this.props;

    return (
    <div>
    {this.state.error && <p>{this.state.error}</p>}
      <form className={classes.container} noValidate autoComplete="off" onSubmit={this.onSubmit}>

           <TextField
        id="name"
        label="Name"
        className={classes.textField}
        value={this.state.name}
        onChange={this.handleChange('name')}
        margin="normal"
      />
            <TextField
        id="location"
        label="location"
        className={classes.textField}
        value={this.state.location}
        onChange={this.handleChange('location')}
        margin="normal"
      />

       <TextField
          id="brand"
          select
          label="Merk"
          className={classes.textField}
          value={this.state.brandId}
          onChange={this.handleChange('brandId')}
          SelectProps={{
            MenuProps: {
              className: classes.menu,
            },
          }}
          helperText="Selecteer een merk"
          margin="normal"
        >
          {
            this.state.brandOptions.map(option => {
              return(
                <MenuItem key={option.key} value={option.value}>
                  {option.label}
                </MenuItem>
            );
            })
          }
        </TextField>

            <TextField
        id="type"
        label="type"
        className={classes.textField}
        value={this.state.type}
        onChange={this.handleChange('type')}
        margin="normal"
      />
            <TextField
        id="size"
        label="size"
        className={classes.textField}
        value={this.state.size}
        onChange={this.handleChange('size')}
        margin="normal"
      />      
      <Button variant="contained"  type="submit" color="primary" className={classes.button}>
          Opslaan
        </Button>
    </form>
    </div>
    )
  }
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
});

AssetTemplateForm.propTypes = {
  brandOptions: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AssetTemplateForm);
/*

  /*
  onAmountChange = (e) => {
    const amount = e.target.value;

    if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
      this.setState(() => ({ amount }));
    }
  };
  onDateChange = (createdAt) => {
    if (createdAt) {
      this.setState(() => ({ createdAt }));
    }
  };
  onFocusChange = ({ focused }) => {
    this.setState(() => ({ calendarFocused: focused }));
  };

 <SingleDatePicker
            date={this.state.createdAt}
            onDateChange={this.onDateChange}
            focused={this.state.calendarFocused}
            onFocusChange={this.onFocusChange}
            numberOfMonths={1}
            isOutsideRange={() => false}
          />
          <textarea
            placeholder="Add a note for your AssetTemplate (optional)"
            value={this.state.note}
            onChange={this.onNoteChange}
          >
          </textarea>

          */

