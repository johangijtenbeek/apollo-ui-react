import React from 'react';
import PropTypes from 'prop-types';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';

class DatagridHead extends React.Component {
    createSortHandler = property => event => {
      this.props.onRequestSort(event, property);
    };
  
    render() {
      const {
          columns, order, orderBy, numSelected, rowCount, selectableRows,
          onSelectAllClick,} = this.props;
      return (
        <TableHead>
          <TableRow>
            {
              selectableRows ? 
              <TableCell padding="checkbox">
                <Checkbox
                  indeterminate={numSelected > 0 && numSelected < rowCount}
                  checked={numSelected === rowCount}
                  onChange={onSelectAllClick}
                />
              </TableCell>
              : null
            }
            {columns.map(col => {
              return (
                <TableCell
                  key={col.propertyName}
                  numeric={col.numeric}
                  padding={col.disablePadding ? 'none' : 'default'}
                  sortDirection={orderBy === col.id ? order : false}
                >
                  <Tooltip
                    title="Sort"
                    placement={col.numeric ? 'bottom-end' : 'bottom-start'}
                    enterDelay={300}
                  >
                    <TableSortLabel
                      active={orderBy === col.propertyName}
                      direction={order}
                      onClick={this.createSortHandler(col.propertyName)}
                    >
                      {col.label}
                    </TableSortLabel>
                  </Tooltip>
                </TableCell>
              );
            }, this)}
          </TableRow>
        </TableHead>
      );
    }
}

DatagridHead.propTypes = {
    columns: PropTypes.array.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

export default DatagridHead;