import React from 'react';
import PropTypes from 'prop-types';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField'
import Checkbox from '@material-ui/core/Checkbox';
import GridControlCell from './DatagridControlCell';

const GridBody = (props) =>
{
    const {rows, emptyRows, selectedRows, columns, classes, onRemove, selectable, clickEvents, changeEvents} = props;

    return (
        <TableBody>
            {
                rows.map(row => {
                    const isSelected = selectedRows.indexOf(row.id) !== -1;
                    return (
                    <TableRow
                        hover
                        onClick={event => clickEvents.handleRowClick(event, row.propertyName)}
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex={-1}
                        key={row.id}
                        selected={isSelected}>
                        { 
                            selectable ? 
                            <TableCell padding="checkbox">
                                <Checkbox checked={isSelected} />
                            </TableCell>                        
                                    : null
                        }                          
                        <TableCell key={`row-${row.rowNumber}`}> 
                            {row.rowNumber}
                        </TableCell>
                        { 
                            columns.map((col, index) => {
                            
                                const hasChange = row.changedColumns.includes(col.propertyName);
                                const textFieldProps = 
                                {
                                    onClick: e => clickEvents.handleCellClick(e),
                                    onChange: e => changeEvents.handleChange(e, col.propertyName, row.id),
                                    value: row[col.propertyName],
                                    label: hasChange ? 'Changed' : ''
                                };
                                

                                    return(
                                    <TableCell key={`${row.id}-${col.propertyName}`}>
                                        <TextField {...textFieldProps}>
                                            {row[col.propertyName]}</TextField>
                                    </TableCell>); 
                                })
                        }
                            
                        <GridControlCell
                            id={row.propertyName}
                            classes={classes}
                            onRemove={onRemove}
                        />
                    </TableRow>
                    );
                })}
                {emptyRows > 0 && (
            <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={6} />
                </TableRow>
            )}
        </TableBody>
    );
}

GridBody.propTypes = {
    rows: PropTypes.array.isRequired, 
    emptyRows: PropTypes.number.isRequired, 
    selectedRows: PropTypes.array.isRequired, 
    columns: PropTypes.array.isRequired, 
    classes: PropTypes.object.isRequired, 
    onRemove: PropTypes.func.isRequired, 
    selectable: PropTypes.bool.isRequired,
    clickEvents: PropTypes.object.isRequired,
    changeEvents: PropTypes.object.isRequired
};

export default GridBody;
