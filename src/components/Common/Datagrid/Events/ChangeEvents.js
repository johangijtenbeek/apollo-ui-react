
export class ChangeEvents{

    constructor(container)
    {
      this.container = container;
    }
  
    handleChange = (e, columnName, rowId) => { 
        const data = this.container.state.data;
        const row = data.find((r) => r.id === rowId);
        
        this.container.setState({
          data: data.map((r) => {
            if (r.id === rowId) {
              return {               
                  ...row,
                [columnName] : e.target.value,
                changedColumns: [...row.changedColumns, columnName] 
              };;
            } else {
              return r;
            }})
        });
      };
  }