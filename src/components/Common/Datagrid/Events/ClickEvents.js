
export class ClickEvents{

    constructor(container)
    {
      this.container = container;
    }

    handleRequestSort = (event, property) => {
      const orderBy = property;
      let order = 'desc';
  
      if (this.container.state.orderBy === property && this.container.state.order === 'desc') {
        order = 'asc';
      }
  
      this.container.setState({ order, orderBy });
    };
  
    handleSelectAllClick = event => {
        if (event.target.checked) {
          this.container.setState(state => ({ selectedRows: state.rows.map(n => n.id) }));
          return;
        }
        this.container.setState({ selectedRows: [] });
      };
    
      handleRowClick = (e, id) => {
        debugger;
        return;
        const { selectedRows } = this.container.state;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];
    
        if (selectedIndex === -1) {
          newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
          newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
          newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
          newSelected = newSelected.concat(
            selected.slice(0, selectedIndex),
            selected.slice(selectedIndex + 1),
          );
        }
    
        this.container.setState({ selectedRows: newSelected });
      };
    
  
      handleCellClick = (e, id) => {
        e.stopPropagation();
      };
  }