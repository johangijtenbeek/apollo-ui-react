
export class PaginationEvents{

  constructor(container)
  {
    this.container = container;
  }

  handleChangePage = (event, page) => {    
    this.container.setState({ page });
  };

  handleChangeRowsPerPage = (event) => {
    this.container.setState({ rowsPerPage: event.target.value });
  };

}