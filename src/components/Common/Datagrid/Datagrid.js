import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import styles from './Style'

import DatagridToolbar from './Components/DatagridToolbar';
import DatagridHead from './Components/DatagridHeader';
import DatagridBody from './Components/DatagridBody';

class Datagrid extends React.Component {      
    render() {   
      const { 
        classes, 
        selectable, emptyRows, columns, rows, rowCount, order = 'asc', orderBy = '', selectedRows, rowsPerPage, page,
        onRemove, 
        paginationEvents, clickEvents, changeEvents
      } = this.props;

      return (
      <Paper className={classes.root}>        
        <DatagridToolbar numSelected={selectedRows.length} />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <DatagridHead
              selectable={selectable}
              columns={columns}
              numSelected={selectedRows.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={clickEvents.handleSelectAllClick}
              onRequestSort={clickEvents.handleRequestSort}
              rowCount={rowCount}
            />
            <DatagridBody 
              selectable={selectable}
              rows={rows}
              selectedRows={selectedRows}
              emptyRows={emptyRows}
              columns={columns}
              classes={classes}
              onRemove={onRemove}
              clickEvents={clickEvents}
              changeEvents={changeEvents}
            />
          </Table>
          <TablePagination
            component="div"
            count={rowCount}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={paginationEvents.handleChangePage}
            onChangeRowsPerPage={paginationEvents.handleChangeRowsPerPage}
          />
        </div>        
      </Paper>
      );
    }
  }
  
  Datagrid.propTypes = {
    rows: PropTypes.array.isRequired,
    rowCount: PropTypes.number.isRequired,
    columns: PropTypes.array.isRequired,
    page: PropTypes.number.isRequired,
    selectable : PropTypes.bool.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    selectedRows: PropTypes.array.isRequired,
    onRemove: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    paginationEvents: PropTypes.object.isRequired,
    clickEvents: PropTypes.object.isRequired,
    changeEvents: PropTypes.object.isRequired,
    emptyRows: PropTypes.number.isRequired,
  };

  
  export default withStyles(styles)(Datagrid);
  