import React from 'react';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import Datagrid from './Datagrid';
import { stableSort } from './HelperMethods';
import { getSorting } from '../Table/HelperMethods';
import { PaginationEvents } from './Events/PaginationEvents';
import { ClickEvents } from './Events/ClickEvents';
import { ChangeEvents } from './Events/ChangeEvents';

class DatagridContainer extends React.Component {

  constructor(props) {
    super(props);   
    this.state = {...props, page: 0};
  }

  render() { 

    const paginationEvents = new PaginationEvents(this);
    const clickEvents = new ClickEvents(this);
    const changeEvents = new ChangeEvents(this);

    const { selectable, columns, data, order, orderBy, rowsPerPage, page } = this.state;

    const rowCount = data.length;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rowCount - page * rowsPerPage);

    let rowCounter = 1;
    
    const rows = data.map((item) => 
    {
      return {rowNumber: rowCounter++, ...item };
    });

    const sortedData = stableSort(rows, getSorting(order, orderBy));  

    const rowForPage = sortedData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

    return (
        <Datagrid
          rows={rowForPage}
          rowCount={rowCount}
          columns={columns}
          page={page}
          rowsPerPage={rowsPerPage}
          selectable={selectable}
          emptyRows={emptyRows}
          selectedRows={[]}
          onRemove={ () => {} }
          paginationEvents = {paginationEvents}
          clickEvents = {clickEvents}
          changeEvents = {changeEvents}
        />
    );
  }
}

DatagridContainer.propTypes = {
  order : PropTypes.string,
  orderBy : PropTypes.string,
  data : PropTypes.array,  
  rowsPerPage : PropTypes.number.isRequired,
  columns: PropTypes.array.isRequired,
  selectable: PropTypes.bool.isRequired  
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(DatagridContainer);


