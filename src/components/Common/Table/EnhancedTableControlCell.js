import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import { Link } from 'react-router-dom';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

const EnhancedTableControlCell = (props) =>
{
    const {id, classes, onRemove} = props;

    return(
    <TableCell>
        <Link to={`/edit/${id}`}><EditIcon className={classes.icon} /></Link>
        <DeleteIcon className={classes.icon} onClick={e => 
            { 
            e.preventDefault();
            e.stopPropagation();
            onRemove(e, id);
            } 
            }/>
    </TableCell> 
    );
}

  
export default EnhancedTableControlCell;