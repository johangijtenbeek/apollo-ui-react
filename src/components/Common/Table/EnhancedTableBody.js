import React from 'react';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import EnhancedTableControlCell from './EnhancedTableControlCell';

const EnhancedTableBody = (props) =>
{
    const {sortedRows, emptyRows, selectedRows, columns, classes, onRemove, selectableRows} = props;

    return (
    <TableBody>
        {
            sortedRows.map(n => {
                const isSelected = selectedRows.indexOf(n.id) !== -1;
                return (
                <TableRow
                    hover
                    onClick={event => this.handleClick(event, n.id)}
                    role="checkbox"
                    aria-checked={isSelected}
                    tabIndex={-1}
                    key={n.id}
                    selected={isSelected}
                >
                    { 
                    selectableRows ? 
                    <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                    </TableCell>                        
                            : null
                    }
                    {  columns.map((col, id) => {
                            return(<TableCell key={id}>{n[col.id]}</TableCell>); 
                        })
                    }
                        
                    <EnhancedTableControlCell
                        id={n.id}
                        classes={classes}
                        onRemove={onRemove}
                    />
                </TableRow>
                );
            })}
            {emptyRows > 0 && (
        <TableRow style={{ height: 49 * emptyRows }}>
                <TableCell colSpan={6} />
            </TableRow>
        )}
    </TableBody>
    );
}
  
export default EnhancedTableBody;