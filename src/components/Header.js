import React from 'react';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';

const Header = () => (
  <header>
    <h1>Apollo Materialen Manager</h1>
    <NavLink to="/" activeClassName="is-active" exact={true}><Button color="primary">Dashboard</Button></NavLink>
    <NavLink to="/assettemplates/create" activeClassName="is-active"><Button color="primary">Materiaal Toevoegen</Button></NavLink>
    <NavLink to="/brands/create" activeClassName="is-active"><Button color="primary">Merk Toevoegen</Button></NavLink>
    <NavLink to="/create" activeClassName="is-active"><Button color="primary">Type Toevoegen</Button></NavLink>
    <NavLink to="/create" activeClassName="is-active"><Button color="primary">Maat Toevoegen</Button></NavLink>
  </header>
);

export default Header;
